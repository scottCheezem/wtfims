package net.theroyalwe.wtfism_android;

/**
 * Created with IntelliJ IDEA.
 * User: scottcheezem
 * Date: 3/27/13
 * Time: 9:01 PM
 * To change this template use File | Settings | File Templates.
 */


/*
"things": [
     {     "type": "tool",
   "id": "1",
   "name": "hammer",
   "containerid": "7",
   "description": "a hammer for hammering things",
   "model": "AH43741",
   "serial": "2",
   "vendorid": "None",
   "sellerid": "None",
   "urlid": "None"},
 */
public class WtfThing {

    private String type;
    private int id;
    private String name;
    private int containerid;
    private String description;
    private String model;
    private int serial;
    private int vendorid;
    private int sellerid;
    private int urlid;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getContainerid() {
        return containerid;
    }

    public void setContainerid(int containerid) {
        this.containerid = containerid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }

    public int getVendorid() {
        return vendorid;
    }

    public void setVendorid(int vendorid) {
        this.vendorid = vendorid;
    }

    public int getSellerid() {
        return sellerid;
    }

    public void setSellerid(int sellerid) {
        this.sellerid = sellerid;
    }

    public int getUrlid() {
        return urlid;
    }

    public void setUrlid(int urlid) {
        this.urlid = urlid;
    }
}
