package net.theroyalwe.wtfism_android;

import android.text.LoginFilter;
import android.util.Base64;
import android.util.Log;
import android.widget.AnalogClock;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 3/15/13
 * Time: 4:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class WtfismApi {
    private static String TAG = "API";
    private static String BASE_URL = "http://10.1.0.32:5000";

    HttpClient client = new DefaultHttpClient();


    private ArrayList<WtfContainer> parseContainers(String jsonContainersString) throws JSONException {
        Log.v(TAG, "working with:"+jsonContainersString);
        ArrayList<WtfContainer> containers = new ArrayList<WtfContainer>();
        JSONObject containersJson = new JSONObject(jsonContainersString);
        JSONArray containersJsonArray = (JSONArray) containersJson.get("containers");

        for(int i = 0 ; i < containersJsonArray.length() ; i++){
            WtfContainer container = new WtfContainer();
            container.setName(((JSONObject)containersJsonArray.get(i)).getString("name"));
            container.setLocationid(((JSONObject)containersJsonArray.get(i)).getInt("locationid"));
            container.setId(((JSONObject)containersJsonArray.get(i)).getInt("id"));
            container.setDescription(((JSONObject)containersJsonArray.get(i)).getString("description"));
            container.setCapabilities(((JSONObject)containersJsonArray.get(i)).getString("capabilities"));
            containers.add(container);

        }


        return containers;

    }

    private ArrayList<WtfLocation> parseLocations(String jsonLocationsString) throws JSONException {
        ArrayList<WtfLocation> locations = new ArrayList<WtfLocation>();
        JSONObject locationsJson = new JSONObject(jsonLocationsString);
        JSONArray locationsJsonArray = (JSONArray)locationsJson.get("locations");

        for (int i = 0 ; i < locationsJsonArray.length() ; i++){
            WtfLocation location = new WtfLocation();
            location.setLat(((JSONObject)locationsJsonArray.get(i)).getDouble("lat"));
            location.setLng(((JSONObject) locationsJsonArray.get(i)).getDouble("lng"));
            location.setId(((JSONObject) locationsJsonArray.get(i)).getInt("id"));
            location.setName(((JSONObject)locationsJsonArray.get(i)).getString("name"));
            location.setDescription(((JSONObject)locationsJsonArray.get(i)).getString("description"));
            location.setAddress(((JSONObject)locationsJsonArray.get(i)).getString("address"));

            locations.add(location);
        }

        return locations;
    }

    private ArrayList<WtfThing> parseThings(String jsonThingsString){
        ArrayList<WtfThing> things = new ArrayList<WtfThing>();

        return things;
    }

    private ArrayList<WtfThingInstance> parseThingInstances(String jsonThingInstanceString) throws JSONException {
        ArrayList<WtfThingInstance> thingInstances = new ArrayList<WtfThingInstance>();
        JSONObject thingInstancesJson = new JSONObject(jsonThingInstanceString);
        JSONArray thingInstancesArray = (JSONArray)thingInstancesJson.get("thingInstances");

        for (int i = 0 ; i < thingInstancesArray.length() ; i++){
            WtfThingInstance thingInstance = new WtfThingInstance();
            thingInstance.setName(((JSONObject)thingInstancesArray.get(i)).getString("name"));
            thingInstance.setDescription(((JSONObject)thingInstancesArray.get(i)).getString("name"));
            thingInstance.setContainerid(((JSONObject)thingInstancesArray.get(i)).getInt("containerid"));
            thingInstance.setContainerReference(((JSONObject)thingInstancesArray.get(i)).getString("containerreference"));
            thingInstance.setQty(((JSONObject)thingInstancesArray.get(i)).getInt("qty"));

            thingInstances.add(thingInstance);


        }

        return thingInstances;
    }

    private String processResponse(HttpResponse response) {

        StringBuilder stringBuilder = new StringBuilder();
        try {

            HttpEntity entity = response.getEntity();
            InputStream stream = entity.getContent();
            int b;
            while ((b = stream.read()) != -1) {
                stringBuilder.append((char) b);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.v(TAG, "RESPONSE:" + stringBuilder.toString());
        return stringBuilder.toString();

    }

    private String makePostRequest(String url,List<NameValuePair> nameValuePairs) throws ClientProtocolException,IOException {

        HttpPost httpPost = new HttpPost(url);
        httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

        return processResponse(client.execute(httpPost));

    }

    private String makeGetRequest(String url) throws ClientProtocolException,
            IOException {

        HttpGet httpGet = new HttpGet(url);

        String response = processResponse(client.execute(httpGet));

        Log.v(TAG, response);
        return response;

    }

    public void submitInventoryItem(String location, String container, String thing, String description, String name, byte[] encode) throws IOException {
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("imgData", Base64.encodeToString(encode, 1)));
        nameValuePairs.add(new BasicNameValuePair("name", name));
        nameValuePairs.add(new BasicNameValuePair("description", description));
        nameValuePairs.add(new BasicNameValuePair("thing", thing));
        nameValuePairs.add(new BasicNameValuePair("container", container));
        nameValuePairs.add(new BasicNameValuePair("location", location));


        //this.makePostRequest(BASE_URL+"/addItem.php", nameValuePairs);
    }


    /*
     //I guess this won't work.or would take to long to figure out right now.
    public void getAllItemsOfType(String type)throws IOException{
        String url = BASE_URL+"/get_all/"+type;




    }
    */



    public void createNewContainer(int location, String description, String name, byte[] encode) throws IOException {
        //location = 1;

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
        nameValuePairs.add(new BasicNameValuePair("imageb64", Base64.encodeToString(encode, 1)));
        nameValuePairs.add(new BasicNameValuePair("name", name));
        nameValuePairs.add(new BasicNameValuePair("location", Integer.toString(location)));
        nameValuePairs.add(new BasicNameValuePair("description", description));

        this.makePostRequest(BASE_URL+"/add/containers", nameValuePairs);

    }

    public ArrayList<WtfContainer> getAllContainers() throws IOException, JSONException {
        String url = BASE_URL+"/get_all/containers";
        Log.v(TAG, "getting "+url);

        String containersResponseString = this.makeGetRequest(url);
        Log.v(TAG, "API returning:"+containersResponseString);

        return parseContainers(containersResponseString);

    }


    /*
    public ArrayList<Object> getAllItemsOfTypeInParent(String type)throws IOException{
        if(type.equals("containers")){

        }else if(type.equals("locations")){

        }else if(type.equals("thinginstances")){

        }else if(type.equals("things")){

        }

    }
    */

    public HashMap<String, Object> getAllWtfObjects() throws IOException, JSONException {
        HashMap<String, Object> all = new HashMap<String, Object>();
        String url = BASE_URL+"/get_all/all";
        Log.v(TAG, "getting "+url);

        String allResponseString = this.makeGetRequest(url);
        Log.v(TAG, "API returning :"+allResponseString);
        all.put("containers", parseContainers(allResponseString));
        //all.put("")
        all.put("locations", parseLocations(allResponseString));
        all.put("thinginstances", parseThingInstances(allResponseString));
        all.put("things", parseThings(allResponseString));

        return all;
    }
}
