package net.theroyalwe.wtfism_android;

/**
 * Created with IntelliJ IDEA.
 * User: scottcheezem
 * Date: 3/27/13
 * Time: 9:02 PM
 * To change this template use File | Settings | File Templates.
 */



/*"containers": [
     {     "locationid": "1",
   "id": "6",
   "name": "woocontainer1",
   "description": "container1 desc",
   "capabilities": "can fly",
   "sellerid": "None"},
*/
public class WtfContainer {
  private int locationid;
    private int id;
    private String name;
    private String description;
    private String capabilities;

    public int getLocationid() {
        return locationid;
    }

    public void setLocationid(int locationid) {
        this.locationid = locationid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(String capabilities) {
        this.capabilities = capabilities;
    }
}
