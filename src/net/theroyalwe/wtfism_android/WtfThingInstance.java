package net.theroyalwe.wtfism_android;

/**
 * Created with IntelliJ IDEA.
 * User: scottcheezem
 * Date: 3/27/13
 * Time: 9:02 PM
 * To change this template use File | Settings | File Templates.
 */



/*"thinginstances": [
     {     "name": "ithoughtidid",
   "description": "hmm",
   "id": "1",
   "thingid": "1",
   "containerid": "1",
   "containerreference": "None",
   "qty": "None"},*/

public class WtfThingInstance {
    private String name;
    private String description;
    private int containerid;
    private String containerReference;
    private int qty;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getContainerid() {
        return containerid;
    }

    public void setContainerid(int containerid) {
        this.containerid = containerid;
    }

    public String getContainerReference() {
        return containerReference;
    }

    public void setContainerReference(String containerReference) {
        this.containerReference = containerReference;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
