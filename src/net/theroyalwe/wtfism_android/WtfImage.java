package net.theroyalwe.wtfism_android;

/**
 * Created with IntelliJ IDEA.
 * User: scottcheezem
 * Date: 4/3/13
 * Time: 8:55 PM
 * To change this template use File | Settings | File Templates.
 */


/*
"images": [
        {
            "type": "locations",
            "id": "1",
            "refid": "31",
            "url": "",
            "description": "already",
            "name": "wtf",
            "localfile": "1"
        },
 */
public class WtfImage {
    private String type;
    private int id;
    private int refid;
    private String url;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;
    private String name;
    private int localfile;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRefid() {
        return refid;
    }

    public void setRefid(int refid) {
        this.refid = refid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLocalfile() {
        return localfile;
    }

    public void setLocalfile(int localfile) {
        this.localfile = localfile;
    }
}
