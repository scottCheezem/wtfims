package net.theroyalwe.wtfism_android;

/**
 * Created with IntelliJ IDEA.
 * User: scottcheezem
 * Date: 3/27/13
 * Time: 9:02 PM
 * To change this template use File | Settings | File Templates.
 */




/*
{     "lat": "40.012737",
   "long": "-83.001371",
   "id": "1",
   "name": "home",
   "description": "cjh home",
   "address": "2413 indiana ave, columbus, OH, 43202"}

 */
public class WtfLocation {
    private double lat;
    private double lng;
    private int id;
    private String name;
    private String description;
    private String address;


    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
